<?php

/*

	Template Name: Menu

*/

get_header(); ?>


	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="menu">
			<div class="wrapper">

				<div class="section-wrapper">

					<div class="section-header">
						<h1><?php the_title(); ?></h1>
					</div>

					<section id="menu-list">
						<?php if(have_rows('menus')): while(have_rows('menus')) : the_row(); ?>

							<div class="menu-item">
								<div class="photo">
									<a href="<?php the_sub_field('pdf_url'); ?>" rel="external">
										<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

										<span class="pdf"><img src="/wp-content/themes/thackeray/images/pdf.svg" alt="PDF" /></span>
									</a>		
								</div>

								<div class="info">
									<a href="<?php the_sub_field('pdf_url'); ?>" rel="external" class="title">
										<?php the_sub_field('title'); ?>
									</a>
								</div>

							</div>
						 
						<?php endwhile; endif; ?>
					</section>

				</div>

			</div>
		</section>

	<?php endwhile; endif; ?>


<?php get_footer(); ?>