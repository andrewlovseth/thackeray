<?php include( locate_template( 'partials/header/global-variables.php', false, false ) ); ?>

<div class="claret">

	<div class="copy">
		<?php the_field('claret_copy'); ?>
	</div>

	<div class="graphics">		
		<div class="arrow">
			<a href="https://claretseattle.com/" rel="external">
				<img src="<?php echo $child_theme_path; ?>/images/arrow-right.svg" alt="Arrow" />			
			</a>
		</div>

		<div class="logo">
			<a href="https://claretseattle.com/" rel="external">
				<img src="<?php $image = get_field('claret_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>
	</div>

</div>