<?php

/*

	Template Name: Landing Page

*/

?>

<!DOCTYPE html>
<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/heavy-restaurants/heavy-restaurants.css" />

	<?php get_template_part('partials/head/styles'); ?>
	
	<?php wp_head(); ?>

</head>

	<body <?php body_class(); ?>>

	<section id="page">
		<div class="photo">
			<img src="<?php $image = get_field('background_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

		<div class="info">
			<div class="info-wrapper">
				<div class="logo">
					<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="headline">
					<h2><?php the_field('headline'); ?></h2>
				</div>

				<div class="copy">
					<?php the_field('copy'); ?>
				</div>


				<div class="ctas">
					<?php if(have_rows('ctas')): while(have_rows('ctas')): the_row(); ?>
					 
					    <div class="cta">
					    	<div class="headline">
					    		<h4><?php the_sub_field('headline'); ?></h4>
					    	</div>

					    	<div class="btn">
					    		<a href="<?php the_sub_field('link'); ?>" rel="external"><?php the_sub_field('link_label'); ?></a>
					    	</div>
					        
					    </div>

					<?php endwhile; endif; ?>					
				</div>		
			</div>
		</div>
	</section>

	<?php include(locate_template('partials/footer/scripts.php')); ?>

	<?php wp_footer(); ?>

</body>
</html>