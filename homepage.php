<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_photo'); echo $image['url']; ?>);">
		
	</section>

	<section id="main">
			
		<section id="photos">

			<div class="gallery">
				<div class="grid col-1">
					<div class="col-2">
						<img src="<?php $image = get_field('top_left_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="col-2">
						<img src="<?php $image = get_field('top_right_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>						
				</div>

				<div class="col-1 wide">
					<img src="<?php $image = get_field('center_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="grid col-1">
					<div class="col-2">
						<img src="<?php $image = get_field('bottom_left_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="col-2">
						<img src="<?php $image = get_field('bottom_right_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>						
				</div>
			</div>
			
		</section>

		<section id="info">
			
			<div class="description">
				<h3><?php the_field('headline'); ?></h3>
				<div class="copy">
					<?php the_field('description'); ?>
				</div>
			</div>

			<div class="next-door desktop">
				<?php get_template_part('partials/claret-teaser'); ?>
			</div>

		</section>

		<div class="next-door mobile">
			<?php get_template_part('partials/claret-teaser'); ?>
		</div>

	</section>


<?php get_footer(); ?>